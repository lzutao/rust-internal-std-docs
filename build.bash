#!/bin/bash

set -eu -o pipefail

ALL_TARGETS=(
    x86_64-unknown-linux-gnu
    x86_64-pc-windows-gnu
    # Artifacts too big to deploy to gitlab pages
    # x86_64-apple-darwin
)

rustup target add "${ALL_TARGETS[@]}"

CARGO_BIN_PATH=$(rustup which cargo)
RUSTC_BIN_PATH=$(rustup which rustc)
# CARGO_BIN_DIR=$(dirname "$CARGO_BIN_PATH")
# STD_SRC="$CARGO_BIN_DIR/../lib/rustlib/src/rust/library/std"
STD_DATE=$(rustc -Vv | grep commit-date | awk '{print $2}')
STD_COMMIT=$(rustc -Vv | grep commit-hash | awk '{print $2}')

export RUSTDOCFLAGS="--document-private-items --crate-version \"${STD_DATE}\" --html-in-header ${PWD}/in-head.html"

mkdir -p public/nightly
cp --archive --link static_root/* public/

# We cannot use `cargo doc` for rust-src itself. Let's fallback to upstream repository.
if [[ ! -v RUST_SRC_REPO ]]; then
    RUST_SRC_REPO=$PWD/rust_repo
    git clone https://github.com/rust-lang/rust "$RUST_SRC_REPO"
    sed \
        -e s#@OUTER_CARGO@#"$CARGO_BIN_PATH"# \
        -e s#@OUTER_RUSTC@#"$RUSTC_BIN_PATH"# \
        xpy_config.toml.in \
        > "$RUST_SRC_REPO"/config.toml \
        ;
fi
pushd "$RUST_SRC_REPO"
    git fetch
    git reset --hard "$STD_COMMIT"
    git submodule update --init --recursive --force
popd

for target in "${ALL_TARGETS[@]}"; do
    cargo doc \
        --target "$target" \
        --target-dir "target" \
        --manifest-path "$RUST_SRC_REPO"/library/std/Cargo.toml \
        ;
    cp --archive --link target/"$target"/doc public/nightly/"$target"
done
